Per this page - https://mtldsg.atlassian.net/wiki/x/QQF9Aw

and [Func Specs](https://mtldsg.atlassian.net/wiki/x/igF9Aw)

* Create 3 Column 1 page app
* Responsive site built for Desktop / Tablet / Mobile
    * Desktop - OS X Chrome & Safari (Firefox if time)
    * Tablet - iPad 3+ Currently available iOS (7.x+)
        * Landscape will load Desktop version
        * Portrait will be responsive
    * Mobile - iPhone 4s+ Currently available iOS (7.x+)
        * Landscape will load a message to rotate to portrait
* Create Contacts widget using JSON
* Create Image Carousel
* Create Modal Overlay